# -*- coding: utf-8 -*-
from djangoplus.ui.components import forms
from djangoplus.decorators import action

from simop.models import Compromisso


class RegistrarQuitacaoForm(forms.ModelForm):

    class Meta:
        model = Compromisso
        fields = 'data_quitacao', 'registro_quitacao'
        title = 'Registro de Quitação de Compromisso'
        submit_label = 'Registrar Quitação'
