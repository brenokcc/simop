# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, date
from django.db import transaction
from djangoplus.admin.models import Unit
from djangoplus.db import models
from django.core.exceptions import ValidationError
from djangoplus.decorators import meta, action, subset, role
from djangoplus.utils.aescipher import encrypt
from djangoplus.utils.mail import send_mail
from enderecos.models import Endereco
from django.conf import settings


@role('cpf')
class Administrador(models.Model):
    cpf = models.CpfField('CPF', search=True, example='111.111.111-11')
    nome = models.CharField('Nome', search=True, example='Rafael Oliveira')

    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
        icon = 'fa-user'
        can_admin = 'Administrador'
        list_shortcut = True
        usecase = 2

    def __str__(self):
        return self.nome


@role('cpf', scope='orgao_set')
class Monitorador(models.Model):
    cpf = models.CpfField('CPF', search=True, example='222.222.222-22')
    nome = models.CharField('Nome', search=True, example='Patrícia do Carmo')

    fieldsets = (
        (u'Dados Gerais', {'fields': ('cpf', 'nome')}),
    )

    class Meta:
        verbose_name = 'Monitorador'
        verbose_name_plural = 'Monitoradores'
        icon = 'fa-users'
        can_admin = 'Administrador'
        list_shortcut = True
        usecase = 3

    def __str__(self):
        return self.nome


class TipoResponsavel(models.Model):
    descricao = models.CharField('Descrição', example='Prefeito')

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',)}),
    )

    class Meta:
        verbose_name = 'Tipo de Responsável'
        verbose_name_plural = 'Tipos de Responsáveis'
        icon = 'fa-sitemap'
        list_shortcut = True
        can_admin = 'Administrador'
        usecase = 4

    def __str__(self):
        return self.descricao


class Periodicidade(models.Model):

    descricao = models.CharField('Descrição', example='Anual')
    qtd_ocorrencias_anuais = models.IntegerField(verbose_name='Qtd. de Ocorrências Anuais', example=1)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'qtd_ocorrencias_anuais')}),
    )

    class Meta:
        verbose_name = 'Periodicidade'
        verbose_name_plural = 'Periodicidades'
        icon = 'fa-clock'
        list_shortcut = True
        usecase = 1

    def __str__(self):
        return self.descricao


class OrgaoFiscalizador(models.Model):

    nome = models.CharField('Nome', example='Receita Federal')

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome',)}),
    )

    class Meta:
        verbose_name = 'Orgão Fiscalizador'
        verbose_name_plural = 'Orgãos Fiscalizadores'
        icon = 'fa-eye'
        list_shortcut = True
        can_admin = 'Administrador'
        usecase = 5

    def __str__(self):
        return self.nome


class TipoObrigacao(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True, example='Declaração de Imposto de Renda')
    dispositivo_legal = models.TextField(verbose_name='Dispositivo Legal', null=True, blank=True, example='Lei 01/2018')
    orgao_fiscalizador = models.ForeignKey(OrgaoFiscalizador, verbose_name='Orgão Fiscalizador', filter=True, example='Receita Federal')
    periodicidade = models.ForeignKey(Periodicidade, verbose_name='Periodicidade', filter=True, example='Anual')
    obrigatoria = models.BooleanField(verbose_name='Obrigatória', help_text='Marque esta opção caso esta obrigação esteja relacionada a todos os clientes.')

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'dispositivo_legal', ('orgao_fiscalizador', 'periodicidade'), 'obrigatoria')}),
    )

    class Meta:
        verbose_name = 'Tipo de Obrigação'
        verbose_name_plural = 'Tipos de Obrigação'
        icon = 'fa-th'
        list_shortcut = True
        can_admin = 'Administrador'
        usecase = 6

    def __str__(self):
        return self.descricao


class Prazo(models.Model):

    descricao = models.CharField(verbose_name='Descrição', null=True, example='JAN/2018')
    referencia = models.CharField(verbose_name='Referência', null=True, example='DEZ/2017')
    fim = models.DateField(verbose_name='Fim', example=date.today)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'referencia', 'fim')}),
    )

    class Meta:
        verbose_name = 'Prazo'
        verbose_name_plural = 'Prazos'
        can_list = 'Administrador'

    def __str__(self):
        return '{} referente à {} até {}'.format(self.descricao, self.referencia, self.fim)


class CronogramaObrigacao(models.Model):
    exercicio = models.IntegerField(verbose_name='Exercício', choices=[[x, x] for x in (2018, 2019)], null=True, example=2018)
    tipo_obrigacao = models.ForeignKey(TipoObrigacao, verbose_name='Tipo de Obrigação', filter=True, example='Declaração de Imposto de Renda')
    prazos = models.OneToManyField(Prazo, verbose_name='Prazos', max=12, count='tipo_obrigacao__periodicidade__qtd_ocorrencias_anuais')

    fieldsets = (
        ('Dados Gerais', {'fields': ('exercicio', 'tipo_obrigacao')}),
        ('Prazos', {'relations': ('prazos',)}),
    )

    class Meta:
        verbose_name = 'Cronograma de Obrigação'
        verbose_name_plural = 'Cronogramas de Obrigaçãos'
        list_shortcut = True
        icon = 'fa-calendar'
        can_admin = 'Administrador'
        usecase = 7

    def __str__(self):
        return '{} ({})'.format(self.tipo_obrigacao, self.exercicio)

    @transaction.atomic()
    def save(self, *args, **kwargs):
        super(CronogramaObrigacao, self).save(*args, **kwargs)
        if self.tipo_obrigacao.periodicidade.qtd_ocorrencias_anuais != self.prazos.count():
            raise ValidationError('O tipo de obrigaçõa "{}" requer a definição de {} prazo(s) no intervalo de um ano.'.format(self.tipo_obrigacao, self.tipo_obrigacao.periodicidade.qtd_ocorrencias_anuais))


class Orgao(Unit):
    nome = models.CharField('Nome', search=True, example='Prefeitura de Macaíba')
    endereco = models.OneToOneField(Endereco, verbose_name='Endereço', null=True, blank=True)
    telefone = models.PhoneField(verbose_name='Telefone', blank=True, null=True, example='(84) 3232-3232')
    email = models.EmailField(verbose_name='E-mail', blank=True, null=True)
    cnpj = models.CnpjField(verbose_name='CNPJ', search=True, example='00.000.000/0001-00')
    monitorador = models.ForeignKey(Monitorador, verbose_name=u'Monitorador', null=True)
    exercicio_atual = models.IntegerField(verbose_name='Exercício Atual', choices=[[x, x] for x in (2018, 2019)], exclude=True, null=True, example=2018)
    ativo = models.BooleanField(verbose_name='Ativo', default=True)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        icon = 'fa-building'
        list_shortcut = True
        can_admin = 'Administrador'
        can_list_by_unit = 'Monitorador'
        add_message = 'Informe os responsáveis por cada área'
        usecase = 8

    fieldsets = (
        ('Dados Gerais', {'fields': (('nome', 'cnpj'), 'monitorador', ('ativo', 'is_em_dia'))}),
        ('Contato/Responsáveis::Contato', {'fields': ('endereco', ('telefone', 'email'))}),
        ('Contato/Responsáveis::Responsáveis', {'relations': ('responsavel_set',)}),
        ('Visualizadores::Visualizadores', {'relations': ('visualizador_set',)}),
        ('Histórico de Obrigações::Exercício Atual', {'fields': ('exercicio_atual',), 'actions': ('gerar_obrigacoes',)}),
        ('Histórico de Obrigações::Obrigações', {'relations': ('obrigacao_set',)}),
    )

    @action('Ativar Cliente', can_execute='Administrador', condition='not ativo', category='Ativacao')
    def ativar(self):
        self.ativo = True
        self.save()

    @action('Inativar Cliente', can_execute='Administrador', condition='ativo', category='Ativacao', style='ajax btn-danger')
    def inativar(self):
        self.ativo = False
        self.save()

    @action('Gerar Obrigações para Exercício', can_execute='Administrador')
    def gerar_obrigacoes(self, exercicio_atual):
        self.exercicio_atual = exercicio_atual
        self.save()
        for cronograma in CronogramaObrigacao.objects.filter(tipo_obrigacao__obrigatoria=True, exercicio=exercicio_atual):
            if not Obrigacao.objects.filter(cliente=self, tipo_obrigacao=cronograma.tipo_obrigacao, exercicio=self.exercicio_atual).exists():
                Obrigacao(cliente=self, tipo_obrigacao=cronograma.tipo_obrigacao, exercicio=self.exercicio_atual).save()

    @meta('Histórico de Notificações')
    def get_notificacoes(self):
        pks = Notificacao.objects.filter(compromisso__obrigacao__cliente=self, contato__isnull=False).order_by(
            'compromisso').values_list('pk', flat=True)
        return Notificacao.objects.filter(pk__in=pks)

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        super(Orgao, self).save(*args, **kwargs)
        if self.monitorador_id:
            self.monitorador.save()

    @meta('Está em dia?', formatter='polegar')
    def is_em_dia(self):
        return not Compromisso.objects.filter(obrigacao__cliente=self).atrasados().exists()


class Responsavel(models.Model):
    cliente = models.ForeignKey(Orgao, verbose_name='Cliente', composition=True)
    tipo = models.ForeignKey(TipoResponsavel, verbose_name='Tipo', example='Prefeito')
    nome = models.CharField(verbose_name='Nome', example='Manoel Paez')
    email = models.EmailField(verbose_name='E-mail', example='manu.paes@gmail.com')
    telefone = models.PhoneField(verbose_name='Telefone', example='(84) 3274-2345')

    fieldsets = (('Dados Gerais', {'fields': ('cliente', ('tipo', 'nome'), ('email', 'telefone'))}),)

    class Meta:
        verbose_name = 'Responsável'
        verbose_name_plural = 'Responsáveis'
        can_admin = 'Administrador'
        can_list_by_unit = 'Monitorador'
        select_display = 'nome', 'telefone', 'email'
        usecase = 9

    def __str__(self):
        return self.nome


@role('cpf', scope='cliente')
class Visualizador(models.Model):
    cliente = models.ForeignKey(Orgao, verbose_name='Cliente', composition=True)
    cpf = models.CpfField('CPF', search=True, example='222.222.222-22')
    nome = models.CharField('Nome', search=True, example='Alexandre Rocha')

    class Meta:
        verbose_name = 'Visualizador'
        verbose_name_plural = 'Visualizador'
        icon = 'fa-users'
        can_admin = 'Administrador'
        usecase = 10

    def __str__(self):
        return self.nome


class ObrigacaoManager(models.DefaultManager):

    @subset('Sem Responsáveis', can_alert=True, can_view='Administrador')
    def sem_responsaveis(self):
        return self.filter(responsaveis_nivel_1__isnull=True, responsaveis_nivel_2__isnull=True, responsaveis_nivel_3__isnull=True)


class Obrigacao(models.Model):
    cliente = models.ForeignKey(Orgao, verbose_name='Cliente', composition=True, filter=True, search=('nome',))
    tipo_obrigacao = models.ForeignKey(TipoObrigacao, verbose_name='Tipo', filter=True)
    exercicio = models.IntegerField(verbose_name='Exercício', choices=[[x, x] for x in (2018, 2019)], null=True, filter=True)
    responsaveis_nivel_1 = models.ManyToManyField(Responsavel, verbose_name='Responsáveis Nível I', related_name='r1', blank=True, example='Rafael Oliveira')
    responsaveis_nivel_2 = models.ManyToManyField(Responsavel, verbose_name='Responsáveis Nível II', related_name='r2', blank=True, example='Rafael Oliveira')
    responsaveis_nivel_3 = models.ManyToManyField(Responsavel, verbose_name='Responsáveis Nível III', related_name='r3', blank=True, example='Manoel Paez')

    fieldsets = (
        ('Dados Gerais', {'fields': ('cliente', 'exercicio',)}),
        ('Obrigação::Tipo', {'relations': ('tipo_obrigacao',)}),
        ('Obrigação::Responsáveis', {'fields': ('responsaveis_nivel_1', 'responsaveis_nivel_2', 'responsaveis_nivel_3'), 'actions': ('identificar_responsaveis',)}),
        ('Agenda de Compromisso::Compromissos', {'relations': ('compromisso_set',)}),
        ('Notificações::', {'relations': ('get_notificacoes',)}),
    )

    class Meta:
        verbose_name = 'Obrigação'
        verbose_name_plural = 'Obrigações'
        can_admin = 'Administrador'
        can_list_by_unit = 'Monitorador'
        can_view_by_unit = 'Monitorador', 'Visualizador'
        icon = 'fa-calendar'
        list_display = 'cliente', 'tipo_obrigacao', 'exercicio', 'responsaveis_nivel_1', 'responsaveis_nivel_2', 'responsaveis_nivel_3'

    def choices(self):
        return dict(
            responsaveis_nivel_1=self.cliente.responsavel_set.all(),
            responsaveis_nivel_2=self.cliente.responsavel_set.all(),
            responsaveis_nivel_3=self.cliente.responsavel_set.all()
        )

    def __str__(self):
        return '{} - {}'.format(self.tipo_obrigacao, self.tipo_obrigacao.periodicidade)

    def save(self, *args, **kwargs):
        qs_cronograma = CronogramaObrigacao.objects.filter(tipo_obrigacao=self.tipo_obrigacao, exercicio=self.exercicio)
        if qs_cronograma.exists():
            super(Obrigacao, self).save(*args, **kwargs)
            qtd_ocorrencias_anuais = self.tipo_obrigacao.periodicidade.qtd_ocorrencias_anuais
            for i, prazo in enumerate(qs_cronograma[0].prazos.all()):
                qs_compromisso = Compromisso.objects.filter(obrigacao=self, sequencial=i)
                if qs_compromisso.exists():
                    compromisso = qs_compromisso[0]
                else:
                    compromisso = Compromisso.objects.create(obrigacao=self, sequencial=i, data_limite=prazo.fim)
                if qtd_ocorrencias_anuais == 1:
                    compromisso.identificacao = 'Parcela Única'
                elif qtd_ocorrencias_anuais == 2:
                    compromisso.identificacao = '{}º Semestre'.format(i + 1)
                elif qtd_ocorrencias_anuais == 3:
                    compromisso.identificacao = '{}º Quadrimestre'.format(i + 1)
                elif qtd_ocorrencias_anuais == 4:
                    compromisso.identificacao = '{}º Trimestre'.format(i + 1)
                elif qtd_ocorrencias_anuais == 6:
                    compromisso.identificacao = '{}º Bimestre'.format(i + 1)
                elif qtd_ocorrencias_anuais == 12:
                    compromisso.identificacao = '{}º Mês'.format(i + 1)
                compromisso.data_limite = prazo.fim
                compromisso.descricao = prazo.descricao
                compromisso.referencia = prazo.referencia
                compromisso.save()
        else:
            raise ValidationError('O cronograma de "{}" ainda não foi definido para o tipo de obrigação "{}".'.format(self.cliente.exercicio_atual, self.tipo_obrigacao))

    @action('Identificar Responsáveis', can_execute='Administrador', inline=True, condition='is_sem_responsavel', choices='choices')
    def identificar_responsaveis(self, responsaveis_nivel_1, responsaveis_nivel_2, responsaveis_nivel_3):
        self.responsaveis_nivel_1.clear()
        for responsavel in responsaveis_nivel_1.all():
            self.responsaveis_nivel_1.add(responsavel)
        for responsavel in responsaveis_nivel_2.all():
            self.responsaveis_nivel_2.add(responsavel)
        for responsavel in responsaveis_nivel_3.all():
            self.responsaveis_nivel_3.add(responsavel)

    @meta('Histórico de Notificações')
    def get_notificacoes(self):
        pks = Notificacao.objects.filter(compromisso__obrigacao=self, contato__isnull=False).order_by('compromisso').values_list('pk', flat=True)
        return Notificacao.objects.filter(pk__in=pks)

    def is_sem_responsavel(self):
        return Obrigacao.objects.all().sem_responsaveis().filter(pk=self.pk).exists()


class CompromissoManager(models.DefaultManager):

    @subset('Próximos', list_display=('obrigacao', 'descricao', 'referencia', 'get_responsaveis_nivel_1'))
    def proximos(self, intervalo=30):
        qs = self.filter(data_quitacao__isnull=True, data_limite__gt=datetime.today())
        if intervalo:
            data_limite = datetime.today() + timedelta(days=intervalo)
            qs = qs.filter(data_limite__lte=data_limite)
        return qs

    @subset('Atrasados', can_view=('Visualizador', 'Monitorador', 'Administrador'), dashboard={'left': 'Visualizador', 'center':'Monitorador'}, list_display=('obrigacao', 'descricao', 'referencia', 'data_limite', 'get_responsaveis_nivel_1', 'get_observacao_ultimo_contato'))
    def atrasados(self):
        return self.filter(data_quitacao__isnull=True, data_limite__lt=datetime.today())


class Compromisso(models.Model):
    obrigacao = models.ForeignKey(Obrigacao, verbose_name='Obrigação', filter=('tipo_obrigacao', 'cliente'), composition=True, search=('cliente__nome',))
    descricao = models.CharField(verbose_name='Período', null=True)
    referencia = models.CharField(verbose_name='Referência')
    data_limite = models.DateField(verbose_name='Vencimento', null=True, filter=True)
    data_quitacao = models.PastDateField(verbose_name='Data da Quitação', null=True, filter=True, exclude=True)
    registro_quitacao = models.FileField(verbose_name='Comprovante de Quitação', upload_to='registros_quitacao', null=True, exclude=True)
    sequencial = models.IntegerField(verbose_name='Sequencial', exclude=True)
    identificacao = models.CharField(verbose_name='Período', null=True)

    fieldsets = (
        ('Dados Gerais', {'relations': ('obrigacao',), 'fields': ( ('descricao', 'referencia'), 'data_limite')}),
        ('Responsáveis::Responsáveis', {'fields': ('get_responsaveis_nivel_1', 'get_responsaveis_nivel_2', 'get_responsaveis_nivel_3')}),
        ('Notificações::Notificações', {'relations': ('get_notificacoes',)}),
        ('Contatos Adicionais::Contatos Adicionais', {'relations': ('contatoadicional_set',)}),
        ('Dados da Quitação::Dados da Quitação', {'fields': ('data_quitacao', 'registro_quitacao'), 'condition': 'data_quitacao'}),

    )

    class Meta:
        verbose_name = 'Compromisso'
        verbose_name_plural = 'Compromissos'
        icon = 'fa-check'
        can_add = 'Administrador'
        can_list = 'Administrador'
        can_list_by_unit = 'Monitorador', 'Visualizador'
        list_lookups = 'obrigacao__cliente'
        list_display = 'obrigacao', 'descricao', 'referencia', 'data_limite', 'data_quitacao'
        list_shortcut = 'Monitorador'
        ordering = 'data_limite',
        list_pdf = True

    def __str__(self):
        return '{} / {}'.format(self.obrigacao.tipo_obrigacao.descricao, self.referencia)

    @action('Alterar Prazo Final', can_execute='Administrador', condition='not data_quitacao', usecase=10.1)
    def definir_data_limite(self, data_limite):
        self.data_limite = data_limite
        self.save()

    @action('Registrar Quitação', can_execute='Administrador', can_execute_by_unit='Monitorador', inline=True, condition='not data_quitacao', icon='fa-check', usecase=15)
    def registrar_quitacao(self, data_quitacao, registro_quitacao):
        self.data_quitacao = data_quitacao
        self.registro_quitacao = registro_quitacao
        self.save()

    @action('Cancelar Quitação', can_execute='Administrador', inline=True, condition='data_quitacao', usecase=16)
    def canelar_quitacao(self):
        self.data_quitacao = None
        self.registro_quitacao = None
        self.save()

    def get_url_registro_quitacao(self):
        return 'http://{}/media/{}'.format(settings.HOST_NAME, self.registro_quitacao)

    def is_atrasado(self):
        return self.data_limite < date.today() and not self.data_quitacao

    @meta('Observação do Último Contato')
    def get_observacao_ultimo_contato(self):
        if self.contatoadicional_set.exists():
            return self.contatoadicional_set.order_by('-pk')[0].observacao
        return None

    def save(self, *args, **kwargs):
        super(Compromisso, self).save(*args, **kwargs)
        if not self.data_quitacao:
            hoje = date.today()
            datas = []
            datas.append((1, (self.data_limite - timedelta(days=15))))
            datas.append((1, (self.data_limite - timedelta(days=10))))
            datas.append((2, (self.data_limite - timedelta(days=10))))
            datas.append((1, (self.data_limite - timedelta(days=5))))
            datas.append((2, (self.data_limite - timedelta(days=5))))
            datas.append((3, (self.data_limite - timedelta(days=5))))
            for tipo in ('E-mail', 'Telefone'):
                for nivel, data_prevista in datas:
                    if data_prevista.isoweekday() == 6:
                        data_prevista = data_prevista - timedelta(days=1)
                    elif data_prevista.isoweekday() == 7:
                        data_prevista = data_prevista + timedelta(days=1)
                    if self.data_limite >= hoje and data_prevista > hoje:
                        qs_notificacao = Notificacao.objects.filter(compromisso=self, tipo=tipo, nivel=nivel, data_prevista=data_prevista)
                        if not qs_notificacao.exists():
                            Notificacao.objects.create(compromisso=self, tipo=tipo, nivel=nivel, data_prevista=data_prevista)

    @meta('Responsáveis Nível 1')
    def get_responsaveis_nivel_1(self):
        return self.obrigacao.responsaveis_nivel_1

    @meta('Responsáveis Nível 2')
    def get_responsaveis_nivel_2(self):
        return self.obrigacao.responsaveis_nivel_2

    @meta('Responsáveis Nível 3')
    def get_responsaveis_nivel_3(self):
        return self.obrigacao.responsaveis_nivel_3

    @meta('Agenda de Notificações')
    def get_notificacoes(self):
        pks = Notificacao.objects.filter(compromisso=self).values_list('pk', flat=True).distinct()
        return Notificacao.objects.filter(pk__in=pks).order_by('tipo', 'data_prevista')


class ContatoAdicional(models.Model):
    compromisso = models.ForeignKey(Compromisso, verbose_name='Compromisso')
    data_hora = models.PastDateTimeField(verbose_name='Data/Hora', default=datetime.now)
    responsavel = models.ForeignKey(Responsavel, verbose_name='Destinatário', null=True)
    efetivado = models.BooleanField(verbose_name='Efetivado', default=False, help_text='Marque essa opção caso o cliente tenha sido comunicado. A pendência relacionada a esse contato será excluída.')
    canal = models.CharField(verbose_name='Canal', null=True, choices=[['Ligação', 'Ligação'], ['SMS', 'SMS'], ['Whatsapp', 'Whatsapp']])
    observacao = models.TextField(verbose_name='Observação', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('compromisso', ('data_hora', 'responsavel'), ('canal', 'efetivado'), 'observacao')}),
    )

    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = 'Contatos'
        can_admin = 'Administrador'
        can_add_by_unit = 'Monitorador'
        add_label = 'Registrar Contato Adicional'
        add_inline = 'atrasados'
        can_view_by_unit = 'Monitorador', 'Visualizador'
        list_lookups = 'compromisso__obrigacao__cliente'
        list_display = 'data_hora', 'responsavel', 'efetivado', 'canal', 'observacao'

    def choices(self):
        qs = Responsavel.objects.all()
        responsaveis = qs.filter(r1=self.compromisso.obrigacao) | qs.filter(r2=self.compromisso.obrigacao) | qs.filter(r3=self.compromisso.obrigacao)
        return dict(responsavel=responsaveis.distinct())

    def __str__(self):
        return '{} - {}'.format(self.data_hora, self.compromisso)

    def can_add(self):
        return self.compromisso_id and self.compromisso.data_limite < date.today()


class NotificacaoManager(models.DefaultManager):

    @subset('Todas', can_view='Superusuário')
    def all(self, *args, **kwargs):
        return super(NotificacaoManager, self).all(*args, **kwargs)

    @subset('Ligações Pendentes', can_view='Monitorador', help_text='Ligações referentes a compromissos não-quitados.', dashboard='bottom')
    def ligacoes_pendentes(self):
        data_minima = datetime.today() - timedelta(days=4)
        return self.filter(data_ciencia__isnull=True, tipo='Telefone', data_prevista__gte=data_minima, data_prevista__lte=datetime.today(), compromisso__data_limite__gte=datetime.today(), compromisso__data_quitacao__isnull=True)

    @subset('E-mails Pendentes', can_view='Administrador', help_text='E-mails que devem ser (re)enviados, pois o compromisso ainda não foi quitado.')
    def emails_pendentes(self):
        return self.filter(tipo='E-mail', data_ciencia__isnull=True, data_prevista__lte=date.today()).distinct()


class Notificacao(models.Model):
    compromisso = models.ForeignKey(Compromisso, verbose_name='Compromisso', filter=('obrigacao__tipo_obrigacao',))
    tipo = models.CharField(verbose_name='Tipo', choices=[[x, x] for x in ('E-mail', 'Telefone')])
    data_prevista = models.DateField(verbose_name='Data Prevista do Contato', null=True, filter=True)
    data_ciencia = models.DateTimeField(verbose_name='Data da Ciência', null=True, exclude=True)
    nivel = models.IntegerField(verbose_name='Nível', null=True, choices=[[1, 'Nível I'], [2, 'Nível II'], [3, 'Nível III']])

    fieldsets = (
        ('Dados Gerais', {'relations': ('compromisso',), 'fields': ('tipo', 'data_prevista', 'data_ciencia', 'nivel')}),
        ('Contatos', {'fields': ('contato_set',)}),
    )

    class Meta:
        verbose_name = 'Notificação'
        verbose_name_plural = 'Notificações'
        icon = 'fa-bell-o'
        can_list = 'Administrador'
        can_list_by_unit = 'Monitorador'
        list_display = 'compromisso', 'compromisso__data_limite', 'tipo', 'data_prevista', 'nivel', 'get_responsaveis', 'get_qtd_contatos_realizadas'
        list_shortcut = 'Administrador'
        list_lookups = 'compromisso__obrigacao__cliente'

    def __str__(self):
        return '{} - {} - {}'.format(self.tipo, self.get_nivel_display(), self.compromisso)

    @meta('Mensagem')
    def get_mensagem(self):
        mensagem = '''
        Prezado(a),

        Gostaríamos de informar que o compromisso <b>{}</b> deverá ser cumprido até o dia <b>{}</b>.

        Para deixar de receber notificação acerca desse compromisso, clique no botão "Registrar Ciência".
        Caso tenha cumprido o compromisso, clique no botão "Registrar Quitação" para realizar o upload do comprovante.
        '''.format(self.compromisso, self.compromisso.data_limite.strftime('%d/%m/%Y'))
        return mensagem

    @meta('Dia')
    def get_dia_semana(self):
        return ('Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom')[self.data_prevista.isoweekday()-1]

    @meta('Destinatários')
    def get_responsaveis(self):
        return getattr(self.compromisso, 'get_responsaveis_nivel_{}'.format(self.nivel))().all()

    @action('Enviar E-mail', inline='emails_pendentes', condition='is_via_email')
    def registrar_contato_por_email(self):
        subject = 'Notificação de Compromisso'
        for responsavel in self.get_responsaveis():
            contato = Contato.objects.create(responsavel=responsavel, data_hora=datetime.now(), notificacao=self, efetivado=False, observacao='{}\n{}'.format(responsavel.email, self.get_mensagem()))
            actions = [
                ('Registrar Ciência', '/simop/registrar_ciencia/{}/'.format(encrypt(contato.pk, True))),
                ('Registrar Quitação', '/simop/registrar_quitacao/{}/'.format(encrypt(contato.pk, True)))
            ]
            send_mail(subject, self.get_mensagem(), responsavel.email, actions=actions)

    def is_via_email(self):
        return self.tipo == 'E-mail'

    def registrar_contato_telefonico_initial(self):
        return dict(
            data_hora=datetime.now()
        )

    def registrar_contato_telefonico_choices(self):
        if self.compromisso_id:
            return dict(
                responsavel=getattr(self.compromisso.obrigacao, 'responsaveis_nivel_{}'.format(self.nivel)).all(),
            )
        else:
            return dict()

    @action('Registrar Contato', can_execute='Administrador', can_execute_by_unit='Monitorador', inline='ligacoes_pendentes', condition='not is_via_email', input='simop.Contato', icon='fa-phone')
    def registrar_contato_telefonico(self, canal, responsavel, data_hora, efetivado, observacao):
        Contato.objects.create(canal=canal, responsavel=responsavel, data_hora=data_hora, notificacao=self, efetivado=efetivado, observacao=observacao)
        if efetivado:
            self.data_ciencia = date.today()
            self.save()

    @meta('Contatos Realizados')
    def get_qtd_contatos_realizadas(self):
        return self.contato_set.filter(data_hora__lt=datetime.now()).count()
    

class Contato(models.Model):
    notificacao = models.ForeignKey(Notificacao, verbose_name='Notificação')
    data_hora = models.PastDateTimeField(verbose_name='Data/Hora')
    responsavel = models.ForeignKey(Responsavel, verbose_name='Destinatário', null=True)
    efetivado = models.BooleanField(verbose_name='Efetivado', default=False, help_text='Marque essa opção caso o cliente tenha sido comunicado. A pendência relacionada a esse contato será excluída.')
    canal = models.CharField(verbose_name='Canal', null=True, exclude=True, choices=[['Ligação', 'Ligação'], ['SMS', 'SMS'], ['Whatsapp', 'Whatsapp']])
    observacao = models.TextField(verbose_name='Observação', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('notificacao', ('data_hora', 'responsavel'), ('canal', 'efetivado'), 'observacao')}),
    )

    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = 'Contatos'
        can_list = 'Administrador'
        can_list_by_unit = 'Monitorador'
        can_view_by_unit = 'Visualizador'
        list_lookups = 'notificacao__compromisso__obrigacao__cliente'

    def __str__(self):
        return '{} - {}'.format(self.data_hora, self.notificacao)

