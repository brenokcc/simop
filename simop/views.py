# -*- coding: utf-8 -*-

from django.http.response import HttpResponseRedirect
from djangoplus.ui.components.calendar import ModelCalendar
from djangoplus.ui.components.navigation.breadcrumbs import httprr
from djangoplus.ui.components.paginator import Paginator
from djangoplus.ui.components.panel import IconPanel, Panel
from djangoplus.utils.aescipher import decrypt

from simop.models import *
from simop.forms import *
from djangoplus.decorators.views import view, action, dashboard


MESES = ('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro')


@dashboard(can_view='Administrador', position='bottom')
def wiget_calendario_notificaoes(request):
    widget = ModelCalendar(request, 'Calendário de Notificações', display_time=False)
    widget.add(Compromisso.objects.all(), 'data_limite', color='#ffa64d')
    widget.add(Notificacao.objects.filter(data_ciencia__isnull=False), 'data_prevista', color='green')
    widget.add(Notificacao.objects.filter(data_ciencia__isnull=True).all(), 'data_prevista', color='#66b3ff')
    return locals()


@dashboard(can_view='Administrador', position='top')
def widget_resumo_clientes(request):
    clientes = []
    meses = MESES
    for cliente in Orgao.objects.all():
        cliente.meses = []
        for i, mes in enumerate(meses):
            qtd_obrigacoes = Compromisso.objects.filter(obrigacao__cliente=cliente, data_limite__month=i+1).count()
            qtd_pendencias = Compromisso.objects.filter(obrigacao__cliente=cliente, data_limite__month=i + 1, data_quitacao__isnull=True, data_limite__lt=date.today()).count()
            cliente.meses.append((qtd_obrigacoes, qtd_pendencias))
        clientes.append(cliente)
    return locals()


@dashboard(can_view='Visualizador', position='top')
def status_cliente(request):
    ok = not Compromisso.objects.all(request.user).atrasados().exists()
    return locals()


@dashboard(can_view='Visualizador', position='center')
def agenda(request):
    qs = Compromisso.objects.all(request.user)
    qs_proximos = qs.proximos()
    qs_atrasados = qs.atrasados().filter()
    widget = ModelCalendar(request, 'Agenda de Compromissos', editable=True, display_time=False, url='/list/simop/compromisso/')
    widget.add(qs_atrasados, 'data_limite', color='#a94442', as_initial_date=True)
    widget.add(qs_proximos, 'data_limite', color='green')
    return locals()


@dashboard(can_view='Visualizador', position='right')
def widgets_relatorio(request):
    widgets = []
    hoje = date.today()

    widget1 = IconPanel(request, 'Relatórios de Obrigação', 'fa-file-pdf-o')
    for i in range(0, hoje.month):
        widget1.add_item('{}/{}'.format(MESES[i], hoje.year), '/simop/relatorio_mensal_obrigacoes/{}/{}/'.format(hoje.year, i+1))
    widgets.append(widget1)

    widget2 = IconPanel(request, 'Relatório de Notificação', 'fa-file-pdf-o')
    for i in range(0, hoje.month):
        qs1 = Contato.objects.all(request.user).filter(data_hora__month=i+1, data_hora__year=hoje.year)
        qs2 = ContatoAdicional.objects.all(request.user).filter(data_hora__month=i+1, data_hora__year=hoje.year)
        if qs1.exists() or qs2.exists():
            widget2.add_item('{}/{}'.format(MESES[i], hoje.year), '/simop/relatorio_mensal_notificacoes/{}/{}/'.format(hoje.year, i+1))
    widgets.append(widget2)
    return locals()


@action(Orgao, 'Relatório de Obrigações', style='ajax pdf')
def relatorio_obrigacoes(request, pk):
    cliente = Orgao.objects.all(request.user).get(pk=pk)
    obrigacoes = []
    for obrigacao in cliente.obrigacao_set.all():
        obrigacao.compromissos = Compromisso.objects.filter(obrigacao=obrigacao)
        obrigacoes.append(obrigacao)
    hoje = date.today()
    return locals()


@view('Relatório Mensal de Notificações', can_view='Visualizador', style='pdf')
def relatorio_mensal_notificacoes(request, ano, mes):
    cliente = Visualizador.objects.get(cpf=request.user.username).cliente
    obrigacoes_contatos = []
    obrigacoes_contatos_adicionais = []
    for obrigacao in cliente.obrigacao_set.all():
        houve_contato = False
        compromissos = Compromisso.objects.filter(obrigacao=obrigacao, data_limite__year=ano,
                                                                    data_limite__month=mes)
        obrigacao.compromissos = []
        for compromisso in compromissos:
            compromisso.contatos = []
            for contato in Contato.objects.filter(notificacao__compromisso=compromisso):
                compromisso.contatos.append(contato)
                houve_contato = True
            obrigacao.compromissos.append(compromisso)
        if houve_contato:
            obrigacoes_contatos.append(obrigacao)

    for obrigacao in cliente.obrigacao_set.all():
        contatos_adicionais = ContatoAdicional.objects.filter(compromisso__obrigacao=obrigacao, data_hora__year=ano,
                                                              data_hora__month=mes)
        if contatos_adicionais.exists():
            obrigacao.contatos_adicionais = contatos_adicionais
            obrigacoes_contatos_adicionais.append(obrigacao)

    hoje = date.today()
    descricao_mes = MESES[int(mes)-1]
    return locals()


@view('Relatório Mensal de Obrigações', can_view=('Visualizador', 'Administrador'), style='pdf')
def relatorio_mensal_obrigacoes(request, ano, mes, pk=None):
    if pk:
        cliente = Orgao.objects.get(pk=pk)
    else:
        cliente = Visualizador.objects.get(cpf=request.user.username).cliente
    compromissos = Compromisso.objects.filter(obrigacao__cliente=cliente, data_limite__year=ano, data_limite__month=mes)
    hoje = date.today()
    descricao_mes = MESES[int(mes)-1]
    return locals()


@view('Compromissos Atrasados', can_view='Administrador')
def compromissos_atrasados(request, ano, mes, pk=None):
    if pk:
        cliente = Orgao.objects.get(pk=pk)
    else:
        cliente = Visualizador.objects.get(cpf=request.user.username).cliente
    compromissos = Compromisso.objects.filter(obrigacao__cliente=cliente, data_limite__year=ano, data_limite__month=mes, data_quitacao__isnull=True)
    paginator = Paginator(request, compromissos, 'Compromissos Atrasados', list_subsets=[])
    return locals()


@view('Registrar Ciência', login_required=False)
def registrar_ciencia(request, token):
    contato = Contato.objects.get(pk=decrypt(token, True))
    contato.notificacao.data_ciencia = datetime.now()
    contato.notificacao.save()
    contato.efetivado = True
    contato.save()
    panel = Panel(request, 'Registro de Ciência', 'Ciência para o compromisso "{}" registrada com sucesso.'.format(contato.notificacao.compromisso))
    return locals()


@view('Registrar Quitação', login_required=False)
def registrar_quitacao(request, token):
    contato = Contato.objects.get(pk=decrypt(token, True))
    form = RegistrarQuitacaoForm(request, instance=contato.notificacao.compromisso)
    form.note = 'Registro de quitação do compromisso {}.'.format(contato.notificacao.compromisso)
    if form.is_valid():
        form.save()
        contato.notificacao.data_ciencia = contato.notificacao.compromisso.data_quitacao
        contato.notificacao.save()
        return httprr(request, '/simop/registrar_quitacao/{}/'.format(token), 'Quitação Registrada com sucesso')
    return locals()

