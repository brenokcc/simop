# -*- coding: utf-8 -*-


def polegar(em_dia, **kwargs):
    color = em_dia and 'green' or '#a94442'
    return '<i class="fa fa-thumbs-{} fa-2x" style="color:{}"></i>'.format(em_dia and 'up' or 'down', color)

