# -*- coding: utf-8 -*-
from os import sep
from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DROPBOX_TOKEN = '' # available at https://www.dropbox.com/developers/apps
DROPBOX_LOCALDIR = MEDIA_ROOT
DROPBOX_REMOTEDIR = '/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME

INSTALLED_APPS += (
    PROJECT_NAME,
    'pessoas',
    'enderecos',
    'endless',
    # 'djangoplus.ui.themes.default',
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

if exists(join(BASE_DIR, 'logs')):
    DEBUG = False
    ALLOWED_HOSTS = ['*']
    HOST_NAME = '189.124.139.92:9000'
    SERVER_EMAIL = 'simop@analisarn.com.br'
    ADMINS = [('Admin', 'brenokcc@yahoo.com.br')]

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_USE_TLS = True
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = 'simop@analisarn.com.br'
    EMAIL_HOST_PASSWORD = 'simop123'
    EMAIL_PORT = 587

    DROPBOX_TOKEN = ''
    BACKUP_FILES = ['media', 'sqlite.db']
else:
    HOST_NAME = 'localhost:8000'


USERNAME_MASK = '000.000.000-00'
DEFAULT_SUPERUSER = '000.000.000-00'
EXTRA_JS = ['/static/js/simop.js']
EXTRA_CSS = ['/static/css/simop.css']

DIGITAL_OCEAN_TOKEN = ''
DIGITAL_OCEAN_SERVER = ''
DIGITAL_OCEAN_DOMAIN = ''

