# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):

    def test(self):
        User.objects.create_superuser('000.000.000-00', None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('', username='000.000.000-00')
    def cadastrar_periodicidade(self):
        self.click_link(u'Periodicidades')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Anual')
        self.enter('Qtd. de Ocorrências Anuais', '1')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='000.000.000-00')
    def cadastrar_administrador(self):
        self.click_link(u'Administradores')
        self.click_button('Cadastrar')
        self.enter('CPF', '111.111.111-11')
        self.enter('Nome', 'Rafael Oliveira')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='111.111.111-11')
    def cadastrar_monitorador(self):
        self.click_link(u'Monitoradores')
        self.click_button('Cadastrar')
        self.enter('CPF', '222.222.222-22')
        self.enter('Nome', 'Patrícia do Carmo')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='111.111.111-11')
    def cadastrar_tiporesponsavel(self):
        self.click_link(u'Tipos de Responsáveis')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Prefeito')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='111.111.111-11')
    def cadastrar_orgaofiscalizador(self):
        self.click_link(u'Orgãos Fiscalizadores')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Receita Federal')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='111.111.111-11')
    def cadastrar_tipoobrigacao(self):
        self.click_link(u'Tipos de Obrigação')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Declaração de Imposto de Renda')
        self.enter('Dispositivo Legal', 'Lei 01/2018')
        self.choose('Orgão Fiscalizador', 'Receita Federal')
        self.choose('Periodicidade', 'Anual')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='111.111.111-11')
    def cadastrar_cronogramaobrigacao(self):
        self.click_link(u'Cronogramas de Obrigaçãos')
        self.click_button('Cadastrar')
        self.choose('Exercício', '2018')
        self.choose('Tipo de Obrigação', 'Declaração de Imposto de Renda')
        self.enter('Descrição', 'JAN/2018')
        self.enter('Referência', 'DEZ/2017')
        self.enter('Fim', '01/04/2018')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='111.111.111-11')
    def cadastrar_orgao(self):
        self.click_link(u'Clientes')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Prefeitura de Macaíba')
        self.enter('CNPJ', '70.187.505/0001-92')
        self.choose('Monitorador', 'Patrícia do Carmo')
        self.enter('Telefone', '(84) 3232-3232')
        self.enter('E-mail', '')
        self.click_button('Cadastrar')
        self.click_icon('Principal')

    @testcase('', username='111.111.111-11')
    def adicionar_responsavel_em_orgao(self):
        self.click_link(u'Clientes')
        self.click_icon('Visualizar')
        self.click_tab('Contato/Responsáveis')
        self.look_at_panel('Responsáveis')
        self.click_button('Adicionar Responsável')
        self.look_at_popup_window()
        self.choose('Tipo', 'Prefeito')
        self.enter('Nome', 'Manoel Paez')
        self.enter('E-mail', 'manu.paez@gmail.com')
        self.enter('Telefone', '(84) 3274-7870')
        self.click_button('Adicionar')
        self.click_icon('Principal')

    def adicionar_visualizador_em_orgao(self):
        self.click_link(u'Clientes')
        self.click_icon('Visualizar')
        self.click_tab('Visualizadores')
        self.look_at_panel('Visualizadores')
        self.click_button('Adicionar Visualizador')
        self.look_at_popup_window()
        self.enter('CPF', '222.222.222-22')
        self.enter('Nome', 'Alexandre Rocha')
        self.click_button('Adicionar')
        self.click_icon('Principal')

    def definir_data_limite_em_compromisso(self):
        self.click_link(u'Clientes')
        self.click_icon('Visualizar')
        self.click_tab('Obrigações')
        self.look_at_panel('Obrigações')
        self.click_icon('Visualizar')
        self.click_tab('Agenda de Compromisso')
        self.look_at_panel('Compromissos')
        self.click_icon('Visualizar')
        self.click_button('Alterar Prazo Final')
        self.look_at_popup_window()
        self.enter('Vencimento', datetime.date.today())
        self.click_button('Alterar Prazo Final')
        self.click_icon('Principal')

    def registrar_quitacao_em_compromisso(self):
        self.click_link(u'Clientes')
        self.click_icon('Visualizar')
        self.click_tab('Histórico de Obrigações')
        self.look_at_panel('Obrigações')
        self.click_icon('Visualizar')
        self.click_tab('Agenda de Compromisso')
        self.look_at_panel('Compromissos')
        self.click_icon('Visualizar')
        self.click_button('Registrar Quitação')
        self.look_at_popup_window()
        self.enter('Data da Quitação', '')
        self.enter('Comprovante de Quitação', '')
        self.click_button('Registrar Quitação')
        self.click_icon('Principal')

    def canelar_quitacao_em_compromisso(self):
        self.click_link(u'Clientes')
        self.click_icon('Visualizar')
        self.click_tab('Obrigações')
        self.look_at_panel('Obrigações')
        self.click_icon('Visualizar')
        self.click_tab('Agenda de Compromisso')
        self.look_at_panel('Compromissos')
        self.click_icon('Visualizar')
        self.click_button('Cancelar Quitação')
        self.click_icon('Principal')
